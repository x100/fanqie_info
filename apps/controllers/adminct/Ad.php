<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 广告管理
 * @author chaituan@126.com
 */
class Ad extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Ad_model'=>'do','admin/AdGroup_model'=>'do_g'));
		$this->load->vars('cat',array('0'=>'请选择','1'=>'栏目','2'=>'商品','3'=>'文章'));
	}
	
	public function index() {
		$this->load->view ('admin/ad/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"title like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data['lid'] = implode(',', $data['lid']);
			is_AjaxResult ( $this->do->add($data));
		} else {
			$data['group'] = $this->do_g->getItems('','','id desc');
			$this->load->view('admin/ad/add',$data);
		}
	}
	
	function edits(){
		if(is_ajax_request()){
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		}
	}
	
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data['lid'] = implode(',', $data['lid']);
			is_AjaxResult($this->do->updates ( $data, "id=" . Posts ( 'id', 'checkid' ) ) );
		} else {
			$data['location'] = get_Cache('location');
			$data['group'] = $this->do_g->getItems('','','id desc');
			$id = Gets("id","checkid");
			$data['item'] = $this->do->getItem("id=$id");
			$this->load->view ( 'admin/ad/edit', $data );
		}
	}
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
}
