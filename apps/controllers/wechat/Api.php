<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
class Api extends CI_Controller {
	
	function ad(){
		if(is_ajax_request()){
			$gid = Posts('gid','num');
			$where = "gid=$gid";
			$this->load->model(array("admin/Ad_model"=>'ad'));
			$ad = $this->ad->getItems($where,'','sort');
			$laba = "分享到微信群，获取更高的权限！";
			if($ad){
				foreach ($ad as $v){
					$v['thumb'] = base_url($v['thumb']);
					$data[] = $v;
				}				
				AjaxResult(1,$laba,$data);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	
	
	function issue_upimg(){
		if(is_ajax_request()){
			$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
			$config = array ("pathFormat" => $path,"maxSize" => 1000, "allowFiles" => array(".gif",".png",".jpg",".jpeg"));
			$this->load->library('Uploader', array ('fileField' =>'file','config' => $config));
			$info = $this->uploader->getFileInfo();
			if($info['state']=='SUCCESS'){				
				AjaxResult(1, '',$info['url']);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function tie_cat(){
		if(is_ajax_request()){
			$uid = isset($_POST['uid'])?$_POST['uid']:0;			
			$this->load->model (array('admin/TieCat_model'=>'do','admin/Shop_model'=>'shop'));
			if($uid){
				$shop = $this->shop->getItem(array('uid'=>$uid),'id');
			}
			$items = $this->do->getItems('','','sort');
			if($items){
				if($uid){
					foreach ($items as $v){
						if($v['id']==8){
							if(!$shop){
								$v['cname'] = $v['cname']."（升级商家，点击免费入住）";
								$v['is_shop'] = 0;
							}else{
								$v['is_shop'] = 1;
							}
						}else{
							$v['is_shop'] = 1;
						}
						$news[] = $v;
					}
					$items = $news;
				}
				
				$items = addimg_url($items, 'icon');
				AjaxResult(1,array('title'=>'湖北最大的信息发布平台','img'=>base_url('res/images/share.jpg')),$items);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function tie() {
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Tie_model'=>'do'));
			$where = '';$order = "addtime desc";
			if(isset($data['uid'])){
				$where = array('uid'=>$data['uid']);
			}else{
				if(isset($data['state'])){//tab 选项搜索
					$cid = '';
					if(isset($data['cid'])){
						$cid = "cid=".$data['cid'];
					}
					
					if($data['state']==0){//最新
						$where = $cid;
					}else if($data['state']==1){//推荐
						$where = "sort=1" . ($cid?" and $cid":'');
						$order = "addtime desc";
					}else if($data['state']==2){//附近
						$scope = calcScope($data['la'], $data['lg'], 500000);//附近50公里范围
						$where = "( latitude between {$scope['minLat']} and {$scope['maxLat']} ) and ( longitude between {$scope['minLng']} and {$scope['maxLng']} ) " . ($cid?" and $cid":'');
					}else if($data['state']==3){//3热帖
						$where = $cid;
						$order = "hits desc";						
					}					
				}			
				
				if(isset($data['search'])){//搜索
					$v = $data['v'];
					$where = "content like '%$v%'";					
					$order = "id desc";
				} 				
			}
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,'',$order,$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){				
				foreach ($items as $v){
					$v['thumb'] = $v['thumb']?explode(',', $v['thumb']):'';
					$v['addtime'] = time_ago($v['addtime']);
					$v['zan'] = $v['zan']?json_decode($v['zan'],true):array();
					$v['comment'] = $v['comment']?json_decode($v['comment'],true):array();
					$v['content'] = str_cut($v['content'], 30);
					$news[] = $v;
				}
				foreach ($news as $v){
					if(is_array($v['thumb'])){
						$thumb = array();
						foreach ($v['thumb'] as $vs){
							$thumb[] = base_url($vs);
						}
					}else{
						$thumb = '';
					}
					$v['thumb'] = $thumb;
					$newss[] = $v;
				}				
				if(isset($data['state'])&&$data['state']==2){//附近人排序
					foreach ($newss as $v){
						$locaArr = calcDistance($data['la'], $data['lg'], $v['latitude'], $v['longitude']);
						$v['sort'] = $locaArr;
						$news_s[] = $v;	
						$sort[] = $locaArr;
					}
					array_multisort($sort,SORT_ASC,$news_s);
					$newss = $news_s;					
				}
				AjaxResult_page($newss,$pagemenu);
			}else{
				AjaxResult_error('暂无数据!~~~~(>_<)~~~~');
			}
		}
	}
	
	function tie_del(){//帖子删除
		if(is_ajax_request()){
			$id = Posts('id');
			$this->load->model(array('admin/Tie_model'=>'do','admin/Zan_model'=>'zan','admin/Comment_model'=>'com'));
			$result = $this->do->deletes(array('id'=>$id));
			if($result){
				$this->zan->deletes(array('tid'=>$id));
				$this->com->deletes(array('tid'=>$id));
			}
			AjaxResult(1, '',$result);
		}
	}
	
	function zancom(){//在家的赞和评论列表
		if(is_ajax_request()){
			$data = Posts();//占时没有分页
			$this->load->model(array('admin/Tie_model'=>'do','admin/Zan_model'=>'zan','admin/Comment_model'=>'com'));
			$tie = $this->do->getItem(array('uid'=>$data['uid']),'id,content');
			$zan = $this->zan->getItems(array('tid'=>$tie['id']),'','id desc');
			$com = $this->com->getItems(array('tid'=>$tie['id']),'','id desc');
			$zancom = array_merge((array)$zan,(array)$com);
			if($zancom){
				foreach ($zancom as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$v['title'] = $tie['content'];
					$news[] = $v;
				}
				AjaxResult(1, '',$news);
			}else{
				AjaxResult_error('暂无数据┭┮﹏┭┮');
			}
		}
	}
	
	function tie_detail(){
		if(is_ajax_request()){
			$id = Posts('id');
			$uid = isset($_POST['uid'])?$_POST['uid']:0;
			$this->load->model(array('admin/Tie_model'=>'do','admin/Card_model'=>'card'));
			$item = $this->do->getItem(array('id'=>$id));
			$card = $this->card->getItem(array('uid'=>$item['uid']),'zz');
			if($item['thumb']){
				$thumb = explode(',', $item['thumb']);
				foreach ($thumb as $v){
					$news[] = base_url($v);
				}	
				$item['thumb'] = $news;
			}
			$is_zan = 0;
			if($item['zan']){
				$zan = json_decode($item['zan'],true);
				foreach ($zan as $v){
					if($v['uid'] == $uid){
						$is_zan = 1;
					}
				}
			}
			$item['is_zan'] = $is_zan;			
			$item['addtime'] = time_ago($item['addtime']);
			$item['zz'] = $card?$card['zz']:'还未创建名片';
			$this->do->updates(array('hits'=>'+=2'),array('id'=>$id));
			AjaxResult(1, '',$item);
		}
	}
	
	function zan(){
		if(is_ajax_request()){
			$data = Posts();
			if(!$data['uid'])AjaxResult_error('请先登录');
			$this->load->model(array('admin/Zan_model'=>'do','admin/Tie_model'=>'tie'));
			$tie = $this->tie->getItem(array('id'=>$data['tid']),'zan');
			$zanArr = $tie['zan']?json_decode($tie['zan'],true):array();
			if($zanArr){
				foreach ($zanArr as $v){
					if($v['uid']==$data['uid']){
						AjaxResult_error('已赞过');
					}
				}
			}
			$data_add = array('tid'=>$data['tid'],'uid'=>$data['uid'],'nickname'=>$data['nickname'],'header'=>$data['header'],'addtime'=>time());
			$result = $this->do->add($data_add);			
			//整合赞			
			if(count($zanArr)<=8){
				array_push($zanArr, array('uid'=>$data['uid'],'h'=>$data['header']));
				$zanEdit = json_encode($zanArr);
				$this->tie->updates(array('zan'=>$zanEdit,'zan_num'=>"+=1"),array('id'=>$data['tid']));
			}
			is_AjaxResult($result);
		}
	}
	
	function zan_lists(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Zan_model'=>'do'));
			$items = $this->do->getItems(array('tid'=>$data['tid']));
			if($items){
				foreach ($items as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$news[] = $v;
				}
				AjaxResult(1, '',$news);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function zan_del(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Zan_model'=>'do','admin/Tie_model'=>'tie'));
			$tie = $this->tie->getItem(array('id'=>$data['tid']),'zan');
			if($tie['zan']){
				//整合赞
				$zanArr = json_decode($tie['zan'],true);
				foreach ($zanArr as $v){
					if($v['uid']==$data['uid']){
						unset($v);
					}
					if(isset($v)){
						$newzan[] = $v;
					}
				}
				$zanEdit = isset($newzan)?json_encode($newzan):'';
				$this->tie->updates(array('zan'=>$zanEdit,'zan_num'=>"-=1"),array('id'=>$data['tid']));
			}
			
			$result = $this->do->deletes(array('tid'=>$data['tid'],'uid'=>$data['uid']));
			is_AjaxResult($result);
		}
	}
	
	function comment(){
		if(is_ajax_request()){
			$data = Posts();			
			if(!$data['uid']||$data['uid']=='NaN')AjaxResult_error('帐号错误，请退出重新进');
			$this->load->model(array('admin/Comment_model'=>'do','admin/Tie_model'=>'tie'));
			$data_add = array('tid'=>$data['tid'],'uid'=>$data['uid'],'nickname'=>$data['nickname'],'header'=>$data['header'],'content'=>$data['content'],'addtime'=>time());
			$result = $this->do->add($data_add);
			$comment = $this->tie->getItem(array('id'=>$data['tid']),'comment');			
			//整合赞			
			$zanArr = $comment['comment']&&$comment['comment']!='null'?json_decode($comment['comment'],true):array();			
			if(count($zanArr)<=5){
				array_push($zanArr, array('uid'=>$data['uid'],'n'=>$data['nickname'],'c'=>str_cut($data['content'], 35),'cid'=>$result));				
				$zanEdit = json_encode($zanArr,JSON_UNESCAPED_UNICODE);				
				$this->tie->updates(array('comment'=>$zanEdit,'comment_num'=>"+=1"),array('id'=>$data['tid']));				
			}
			if($result){
				$data_add['id'] = $result;
				$data_add['addtime'] = time_ago($data_add['addtime']);
				AjaxResult(1, 'ok',$data_add);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function comment_huifu(){
		if(is_ajax_request()){
			$data = Posts();
			if(!$data['uid'])AjaxResult_error('请先登录');
			$this->load->model(array('admin/Comment_model'=>'do'));
			$data_add = array('tid'=>$data['tid'],'uid'=>$data['uid'],'nickname'=>$data['nickname'],'header'=>$data['header'],'content'=>$data['content'],'addtime'=>time(),'b_uid'=>$data['buid'],'b_nickname'=>$data['bnickname']);
			$result = $this->do->add($data_add);
			if($result){
				$data_add['id'] = $result;
				$data_add['addtime'] = time_ago($data_add['addtime']);
				AjaxResult(1, 'ok',$data_add);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function comment_del(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Comment_model'=>'do','admin/Tie_model'=>'tie'));
			$tie = $this->tie->getItem(array('id'=>$data['tid']),'comment');
			//整合赞
			if($tie['comment']){
				$zanArr = json_decode($tie['comment'],true);
				foreach ($zanArr as $v){
					if($v['cid']==$data['cid']){
						unset($v);
					}
					if(isset($v)){
						$newzan[] = $v;
					}
				}
				$zanEdit = isset($newzan)?json_encode($newzan):'';
				$this->tie->updates(array('comment'=>$zanEdit,'comment_num'=>"-=1"),array('id'=>$data['tid']));
			}
			
			$result = $this->do->deletes(array('id'=>$data['cid'],'uid'=>$data['uid']));
			is_AjaxResult($result);
		}
	}
	
	function comment_lists(){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Comment_model'=>'do'));
			$items = $this->do->getItems(array('tid'=>$data['tid']));
			
			if($items){
				foreach ($items as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$news[] = $v;
				}
				AjaxResult(1, '',$news);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function city(){
		if(is_ajax_request()){			
			$this->load->model(array('admin/City_model'=>'do'));
			$items = $this->do->getItems('','id,cname as text,parent_id','id');
			$this->load->library ('Tree');
			$data = $this->tree->makeTree ( $items );
			AjaxResult(1, '',$data);
		}
	}
	
	function industry(){
		if(is_ajax_request()){
			$this->load->model(array('admin/Industry_model'=>'do'));
			$items = $this->do->getItems('','id,cname as text,parent_id','id');
			$this->load->library ('Tree');
			$data = $this->tree->makeTree ( $items );
			AjaxResult(1, '',$data);
		}
	}
	

}
