<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );
/**
 * 店铺独立访客
 * @author chaituan@126.com
 */
class ShopUv_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'shop_uv';
	}
}