<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">配置</div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
		<?php foreach($items as $v) {if($v['imports'] == 'text') {?>
			<div class="layui-form-item">
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-inline">
					<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
					<input type="text" name="data[<?php echo $v['id'];?>][val]" value="<?php echo $v['val'];?>" class="layui-input" placeholder="<?php echo $v['cname']?>" lay-verify="required">
				</div>
				<div class="layui-form-mid layui-word-aux"><?php echo $v['bak']?></div>
			</div>
        <?php }if($v['imports'] == 'textarea'){?>
        	<div class="layui-form-item">
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-inline">
					<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
					<textarea class="layui-textarea" name="data[<?php echo $v['id'];?>][val]" max-length="100" placeholder="<?php echo $v['cname']?>" lay-verify="required"><<?php echo $v['val'];?></textarea>
				</div>
			</div>
        <?php }else{?>
            <div class="layui-form-item">
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-inline">
					<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
					<input type="checkbox" name="data[<?php echo $v['id'];?>][val]" value="1" lay-skin="switch" lay-text="开启|关闭" <?php if($v['val']==1){ echo 'checked';}?>> <i></i>
				</div>
				<div class="layui-form-mid layui-word-aux"><?php echo $v['bak']?></div>
			</div>
		<?php }}?>
            <div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('adminct/config/quicksave'),'save','layui-btn-lg',"lay-filter='sub' location=''")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>
