<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title><?php echo SYSTEM_NAME;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="chaituan@126.com">
<link rel="stylesheet" href="<?php echo LAYUI."css/layui.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."admin/main.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."font-awesome.min.css";?>" type="text/css" />
<?php  if(isset($definedcss)){   foreach ($definedcss as $v){?>
	<link type="text/css" href="<?php echo $v.'.css'?>" rel="stylesheet" />
<?php }}?>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
		<!-- 顶部 -->
		<div class="layui-header header">
			<div class="layui-main">
				<a href="<?php echo site_url('adminct/manager/index')?>" class="logo"><?php echo SYSTEM_NAME;?></a>
				<!-- 显示/隐藏菜单 -->
				<a href="javascript:;" class="iconfont hideMenu icon-menu1"><i class="fa fa-bars"></i></a>
				
				<!-- 搜索 -->
				<div class="layui-form component">
			        <select name="modules" lay-verify="required" lay-search="">
						<option value="">直接选择或搜索选择</option>
						<option value="1">待开发</option>
			        </select>
			        <i class="layui-icon">&#xe615;</i>
			    </div>
			   
			    <!-- 顶部右侧菜单 -->
			    <ul class="layui-nav top_menu">
			    	<li class="layui-nav-item showNotice" id="showNotice" pc>
						<a href="javascript:;"><i class="fa fa-fw fa-bullhorn"></i><cite>系统公告</cite></a>
					</li>
			    	<li class="layui-nav-item" mobile>
			    		<a href="javascript:;" class="mobileAddTab" data-url="page/user/changePwd.html"><i class="iconfont icon-shezhi1" data-icon="icon-shezhi1"></i><cite>设置</cite></a>
			    	</li>
			    	<li class="layui-nav-item" mobile>
			    		<a href="<?php echo site_url('adminct/login/logout')?>" class="signOut"><i class="iconfont icon-loginout"></i> 退出</a>
			    	</li>
					<li class="layui-nav-item lockcms" pc>
						<a href="javascript:;"><i class="fa fa-fw fa-lock"></i><cite>锁屏</cite></a>
					</li>
					<li class="layui-nav-item" pc>
						<a href="javascript:;">
						<i class="fa fa-fw fa-address-card"></i>
							<cite><?php echo $loginUser['username']?></cite>
						</a>
						<dl class="layui-nav-child">
							<dd><a href="javascript:;" class="edit_pwd"><i class="iconfont icon-shezhi1" data-icon="icon-shezhi1"></i><cite>修改密码</cite></a></dd>
							<dd><a href="javascript:;" class="changeSkin"><i class="iconfont icon-huanfu"></i><cite>更换皮肤</cite></a></dd>
							<dd><a href="<?php echo site_url('adminct/login/logout')?>" class="signOut"><i class="iconfont icon-loginout"></i><cite>退出系统</cite></a></dd>
						</dl>
					</li>
				</ul>
			</div>
		</div>