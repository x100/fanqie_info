<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入名称" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='是|否' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {field: 'id', title: 'ID', width: 80,sort:true},
	       {field: 'thumb', title: '头像',toolbar:'<div><div class="img_view"><img src="{{d.thumb}}"></div></div>'},
	       {field: 'sname', title: '商家',edit:'text'},
	       {field: 'username', title: '帐号',edit:'text'},
	       {field: 'zb', title: '坐标',edit:'text'},
	       {field: 'address', title: '地址',edit:'text'},
	       {field: 'mobile', title: '电话',edit:'text'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
layui.table.on('edit(common)', function(obj){
    var data = {id:obj.data.id},key = "data["+obj.field+"]";
    	data[key] = obj.value;
    $.post('<?php echo site_url("$dr_url/edits")?>',data,function(d){layer.msg(d.message)},'json');
});
</script>
<?php echo template('admin/footer');?>