<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_NAME;?></title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="<?php echo LAYUI."css/layui.css";?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH."admin/main.css";?>" type="text/css" />
</head>
<body class="childrenBody">
	<div style="text-align: center; padding:11% 0;">
		<svg class="icon" aria-hidden="true" style="font-size:18rem; color: #393D50;"><use xlink:href="#icon-4042"></use></svg>
		<p style="font-size: 20px; font-weight: 300; color: #999;margin-top: 5px;">好干净的页面啊，什么都没有的样子</p>
	</div>
	<div style="position: fixed;left: 0px;right: 0;bottom: 0;height: 44px;line-height: 44px;padding: 0 15px;background-color: #eee;text-align: center;">
	<p>© 2016 - 2017 &nbsp;&nbsp; <a href="http://www.chaituans.com/" target="_blank">襄阳市番茄网络科技有限公司 &nbsp;&nbsp;</a> V3.0</p>
	</div>
	<script type="text/javascript" src="<?php echo JS_PATH.'icon.js'?>" ></script>
</body>
</html>