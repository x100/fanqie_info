<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $item['title'];?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />
	
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/vant.min.css";?>">
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/style.css";?>">
	<style>
		img{width: 100%;}
	</style>
</head>
<body>
<div id="app">
	<van-row class="g_ff">		
		<van-col span="24">
		  	<?php echo $item['content'];?>
		</van-col>
	</van-row>
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app'
});
</script>
</body>
</html>