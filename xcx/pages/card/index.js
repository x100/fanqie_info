import Poster from '../../miniprogram_dist/poster/poster';
const app = getApp();
Page({
  data: {

  },
  onLoad: function (options) {
    
  },
  onReady: function () {

  },
  onShow: function () {
    if (app.reg()) {           
      this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
      this.get_card();     
    } 
  },
  get_card:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card',
      data: {uid:this.data.uid},
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
         this.setData({card:d.data.data});
          wx.setStorageSync('mycard', d.data.data);
        }
        wx.hideLoading();
      }
    });
  },
  addcard:function(){
    this.setData({edit:0});
    wx.navigateTo({
      url: '/pages/card/edit/index?id='+this.data.card.id,
    })
  },
  issue:function(){
    this.setData({ edit: 0 });
    wx.navigateTo({
      url: '/pages/issue/index',
    })
  },
  cardtop:function(){
    this.setData({ edit: 0 });
    wx.switchTab({
      url: '/pages/card/top/index',
    })
    
  },
  zan:function(){
    this.setData({ edit: 0 });
    wx.navigateTo({
      url: '/pages/card/zan/index',
    })
  },
  follow:function(){
    this.setData({ edit: 0 });
    wx.navigateTo({
      url: '/pages/card/follow/index',
    })
  },
  rq: function () {
    this.setData({ edit: 0 });
    wx.navigateTo({
      url: '/pages/card/rq/index',
    })
  },
  onPosterSuccess(e) {
    const { detail } = e;
    wx.previewImage({
      current: detail,
      urls: [detail]
    })
  }, 
  onPosterFail(err) {
    console.error(err);
  },
  /**
     * 异步生成海报
     */
  onCreatePoster() {
    wx.showModal({
      title: '提示',
      content: '生成海报后长按点击保存相册，然后就可以发布朋友圈了',
      showCancel:false,
      confirmText:'立即生成',
      success:(res)=>{
        if (res.confirm){
          wx.request({
            url: app.globalData.host + 'wechat/home/card_hb',
            data: {id:this.data.card.id},
            method: 'POST',
            header: app.header(wx.getStorageSync('PHPSESSID')),
            success: d => {
              app.again(d.data.state);
              if (d.data.state == 1) {
                this.setData({qrcode:d.data.data});
                this.hbconfig();                
                Poster.create();
              } else {
                wx.showToast({
                  title: d.data.message, icon: 'none'
                })
              }
              wx.hideLoading();
            }
          });          
        }        
      }
    })   
  }, 
  hbconfig:function(){
    this.setData({
      posterConfig: {
        width: 750,
        height: 770,
        backgroundColor: '#a6a6a6',
        debug: false,
        blocks: [
          { width: 690, height: 320, x: 30, y: 50, backgroundColor: '#fff', borderRadius: 20 },
          { width: 690, height: 300, x: 30, y: 420, backgroundColor: '#fff', borderRadius: 20 }
        ],
        texts: [
          { x: 70, y: 110, baseLine: 'middle', text:this.data.card.username, fontSize: 30, color: '#000', zIndex: 100 },
          {
            x: 70, y: 160, baseLine: 'middle', zIndex: 100,
            text: [
              { text: this.data.card.bm, fontSize: 25, color: '#969696' },
              { text: this.data.card.position, fontSize: 25, color: '#969696', marginLeft: 30 }
            ]
          },
          { x: 70, y: 220, baseLine: 'middle', text: '电话：' + this.data.card.mobile, fontSize: 25, color: '#666', zIndex: 100 },
          { x: 70, y: 260, baseLine: 'middle', text: '公司：' + this.data.card.company, fontSize: 25, color: '#666', zIndex: 100 },
          { x: 70, y: 300, baseLine: 'middle', text: '地址：' + this.data.card.address, fontSize: 25, color: '#666', zIndex: 100, width:600 },
          { x: 130, y: 670, baseLine: 'middle', text: '你好这是我的名片，请长按二维码识别惠存', fontSize: 25, color: '#969696', zIndex: 100 },
        ],
        images: [
          { width: 120, height: 120, x: 585, y: 90, borderRadius: 120, url: this.data.card.thumb, zIndex: 100 },
          { width: 200, height: 200, x: 268, y: 450,url: this.data.qrcode, zIndex: 100 }
        ]
      }
    });
      
  },
  onShareAppMessage:function(){
    return {
      title: '你好，这是我的名片，请惠存',
      path: '/pages/card/top/detail?id=' + this.data.card.id 
    }
  }
})