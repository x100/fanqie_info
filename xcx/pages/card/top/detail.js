const app = getApp();
Page({

  data: {

  },
  onLoad: function (options) {
    var uid = parseInt(wx.getStorageSync('FXID'));
    var id = 0 ;
    if (options){
      id = options.scene ? decodeURIComponent(options.scene) : options.id;
    }else{
      id = this.data.id;
    }
    this.setData({ id: id, uid: uid });
    this.get_card();
  },
  onReady: function () {

  },
  onShow: function () {
    
  },
  get_card: function () {
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_top_detail',
      data: { id: this.data.id ,uid:this.data.uid },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ card: d.data.data });
          this.get_tie();
        }else{
          wx.showModal({
            showCancel: false,
            content: d.data.message,
            success: res => {
              wx.navigateBack({});
            }
          })      
        }
        wx.hideLoading();
      }
    });
  },
  get_tie:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_top_detail_tie',
      data: { uid: this.data.card.uid },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ tie: d.data.data });
        } else {
          this.setData({msg:d.data.message});
        }
        wx.hideLoading();
      }
    });
  },
  onDetail:function(e){
    var id = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/index/detail/index?id=' + id + "&bottom=0",
    })
  },
  zan:function(){
    if (!app.reg()) return false;
    wx.showLoading({ title: '点赞中...', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/zan',
      data: { uid: this.data.uid,o_uid:this.data.card.uid,o_position:this.data.card.position,query:'add' },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var tie = "card.is_zan", zan = "card.zan";
          this.setData({ [tie]: 1, [zan]: parseInt(this.data.card.zan)  + 1 });
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    }) 
  },
  zanDel: function () {
    wx.showLoading({ title: '取消中...', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/zan',
      data: { uid: this.data.uid, o_uid: this.data.card.uid, query: 'del' },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var tie = "card.is_zan", zan = "card.zan";
          this.setData({ [tie]: 0, [zan]: parseInt(this.data.card.zan) - 1 });
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    })
  },
  follow:function(){
    if (!app.reg()) return false;
    wx.showLoading({ title: '关注中...', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/follow',
      data: { uid: this.data.uid, o_uid: this.data.card.uid, o_position: this.data.card.position, query: 'add' },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var tie = "card.is_follow", follow = "card.follow";
          this.setData({ [tie]: 1, [follow]: parseInt(this.data.card.follow) + 1 });
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    }) 
  },
  followDel:function(){
    wx.showLoading({ title: '取消关注...', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/follow',
      data: { uid: this.data.uid, o_uid: this.data.card.uid, query: 'del' },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var tie = "card.is_follow",follow = "card.follow";
          this.setData({ [tie]: 0, [follow]:parseInt(this.data.card.follow) - 1 });
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    })
  },
  back:function(){
    wx.navigateTo({
      url: '/pages/card/index',
    });
  },
  mobile:function(){
    wx.addPhoneContact({
      firstName: this.data.card.username,
      remark:this.data.card.zz,
      mobilePhoneNumber:this.data.card.mobile,
      addressState:this.data.card.address,
      organization:this.data.card.company,
      title:this.data.card.position
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: this.data.card.cname + '的名片，请惠存',
      path: '/pages/card/top/detail?id=' + this.data.card.id
    }
  }
})