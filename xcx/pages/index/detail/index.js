const app = getApp();
var imgshow = false;
Page({
  data: {
    zan:[],
    comment:[],
    images:{}
  },
  onLoad: function (options) {
    this.setData({ 
      id: options ? options.id:this.data.id, 
      bottom: options ? options.bottom:this.data.bottom, 
      user: wx.getStorageSync('userinfo'), 
      uid: wx.getStorageSync('FXID') ? parseInt(wx.getStorageSync('FXID')) : 0 
    });  
          
  },  
  onShow: function () {
    if (!imgshow) {
      this.get_item();
      this.get_comment();      
    }
  },
  onReady: function () {
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    this.setData({
      animationData: animation.export()
    })
    var n = 0;
    //连续动画需要添加定时器,所传参数每次+1就行
    setInterval(function () {
      n = n + 1;      
      this.animation.rotate(180 * (n)).step();
      this.setData({
        animationData: this.animation.export()
      })
    }.bind(this), 1000)

    
    if(this.data.bottom==1){
      setTimeout(() => {
        this.pageScrollToBottom();
      }, 1000); 
    }       
  },
  get_item:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/api/tie_detail',
      data:{id:this.data.id,uid:this.data.uid},
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        this.setData({ tieDetail: d.data.data });
        wx.hideLoading();
      }
    });
  },
  onMap: function (e) {
    wx.openLocation({
      latitude: parseFloat(e.currentTarget.dataset.la),
      longitude: parseFloat(e.currentTarget.dataset.lg),
      address: e.currentTarget.dataset.name
    })
  },
  onTab: function (e) {
    var index = e.detail.index;
    if (index == 1) {
      this.get_zan();
    } else {
      this.get_comment();
    }
  },
  get_comment:function(){
    if (this.data.comment.length) return false;
    wx.showLoading({ title: '评论加载中', mask: true });   
    wx.request({
      url: app.globalData.host + 'wechat/api/comment_lists',
      data: { tid: this.data.id },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({comment:d.data.data});
        } 
        wx.hideLoading();
      }
    })     
  },
  get_zan:function(){
    if (this.data.zan.length) return false;
    wx.showLoading({ title: '点赞加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/api/zan_lists',
      data: { tid: this.data.id },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ zan: d.data.data });
        }
        wx.hideLoading();
      }
    })
  },
  onZan:function(){    
    if (!app.reg()) return false;
    wx.showLoading({ title: '点赞中...', mask: true });   
    wx.request({
      url: app.globalData.host + 'wechat/api/zan',
      data: { tid: this.data.tieDetail.id, uid: this.data.uid, header: this.data.user.avatarUrl, nickname: this.data.user.nickName },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var tie = "tieDetail.is_zan";
          this.data.zan.push({ header: this.data.user.avatarUrl, nickname: this.data.user.nickName,uid:this.data.uid })
          this.setData({ [tie]: 1, zan: this.data.zan});
          this.pageScrollToBottom();
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    })   
  },
  onZandel: function () {
    var uid = this.data.uid;
    wx.showModal({
      title: '提示',
      content: '确认要取消吗？',
      success:(res)=>{
        if(res.confirm){
          wx.showLoading({ title: '取消中...', mask: true });
          wx.request({
            url: app.globalData.host + 'wechat/api/zan_del',
            data: { tid: this.data.tieDetail.id, uid: this.data.uid },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
            success: d => {
              if (d.data.state == 1) {
                var tie = "tieDetail.is_zan";
                var newslists = this.data.zan.filter(function (item) {
                  return item.uid != uid;
                });
                this.setData({ [tie]: 0, zan: newslists.length ? newslists : [] });
              } else {
                wx.showToast({
                  title: d.data.message,
                })
              }
              wx.hideLoading();
            }
          })
        }
      }
    })    
  },
  onImg: function (e) {
    imgshow = true;
    wx.previewImage({
      current: e.currentTarget.dataset.src,
      urls: this.data.tieDetail.thumb
    })
  },
  onPinlun: function () {    
    if (!app.reg()) return false;
    this.setData({  showpopup: true, plinput: true });
  },
  onHuifu: function (e) {
    if (!app.reg()) return false;
    var hf = e.currentTarget.dataset;
    this.setData({ showpopuphf: true, plinputs: true,hf:hf });
  },
  onClosepop: function () {
    this.setData({ showpopup: false,showpopuphf: false })
  },
  onCommentsub: function (e) {
    if (!app.reg()) return false;
    wx.showLoading({ title: '提交中...', mask: true });
    var con = e.detail;    
    wx.request({
      url: app.globalData.host + 'wechat/api/comment',
      data: { tid: this.data.tieDetail.id, uid: this.data.uid, header: this.data.user.avatarUrl, nickname: this.data.user.nickName, content: con },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.pageScrollToBottom();
          this.data.comment.push(d.data.data)
          this.setData({ comment: this.data.comment, showpopup: false, plinput: false, commentVal: '' });
        } else {
          wx.showToast({
            title: d.data.message, icon: 'none'
          })
        }
        wx.hideLoading();
      }
    })
  },
  onHuifusub: function (e) {
    if (!app.reg()) return false;
    wx.showLoading({ title: '提交中...', mask: true });
    var con = e.detail;
    wx.request({
      url: app.globalData.host + 'wechat/api/comment_huifu',
      data: { tid: this.data.tieDetail.id, uid: this.data.uid, header: this.data.user.avatarUrl, buid: this.data.hf.buid, bnickname: this.data.hf.bnickname, nickname: this.data.user.nickName, content: con },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.pageScrollToBottom();
          this.data.comment.push(d.data.data);
          this.setData({ comment: this.data.comment, showpopuphf: false, plinputs: false, commentVal: '' });
        } else {
          wx.showToast({
            title: d.data.message, icon: 'none'
          })
        }
        wx.hideLoading();
      }
    })
  },
  onCommentdel: function (e) {
    var uid = this.data.uid;
    var cid = e.currentTarget.id;
    wx.showModal({
      title: '提示',
      content: '确认要删除吗？',
      success:  (res)=> {
        if (res.confirm) {
          wx.showLoading({ title: '删除中...', mask: true });
          wx.request({
            url: app.globalData.host + 'wechat/api/comment_del',
            data: { tid: this.data.tieDetail.id, uid: this.data.uid, cid: cid },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
            success: d => {
              if (d.data.state == 1) {
                var newslists = this.data.comment.filter(function (item) {
                  return item.id != cid;
                });
                this.setData({ comment: newslists.length ? newslists : [] });
              } else {
                wx.showToast({
                  title: d.data.message,
                })
              }
              wx.hideLoading();
            }
          })
        } else if (res.cancel) {
          
        }
      }
    })
    
  },
  pageScrollToBottom: function () {
    wx.createSelectorQuery().select('#j_page').boundingClientRect(function (rect) {
      wx.pageScrollTo({
        scrollTop: rect.height,
        duration: 1000
      })
    }).exec()
  },
  onIndex:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})