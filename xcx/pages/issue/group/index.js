const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lists:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_lists();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  get_lists: function () {
    var uid = parseInt(wx.getStorageSync('FXID'));
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/api/tie_cat',
      data:{uid:uid},
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({lists:d.data.data});
        }
        wx.hideLoading();
      }
    })
  },
  select: function (e) {
    var pages = getCurrentPages();
    var prevpage = pages[pages.length - 2];
    prevpage.setData({ cat_info: { id: e.currentTarget.id, cname: e.currentTarget.dataset.cname } });
    wx.navigateBack({})
  },
  shengji:function(e){
    wx.navigateTo({
      url: '/pages/shop/issue/index',
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

})