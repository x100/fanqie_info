const app = getApp();
Page({

  data: {

  },
  onLoad: function (options) {
    var uid = wx.getStorageSync('FXID') ? parseInt(wx.getStorageSync('FXID')) : 0;
    this.setData({ id: options?options.id:this.data.id, uid: uid});
    this.get_detail();      
  },
  onReady: function () {

  },
  onShow: function () {
    
  },
  get_detail:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_detail',
      data:{id:this.data.id,uid:this.data.uid},
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if(d.data.state==1){
          this.setData({ detail: d.data.data,is_sc:d.data.data.is_sc });
          this.get_tie();
        }
        wx.hideLoading();
      }
    });
  },
  sc:function(){
    if (!app.reg()) return false;
    wx.showLoading({ title: '玩命收藏中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_sc',
      data: { id: this.data.id, uid: this.data.uid, o_uid: this.data.detail.uid, query:'add',sid:this.data.detail.id,content:this.data.detail.content
        , sname: this.data.detail.sname ,header:this.data.detail.header
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ is_sc: 1 });
        }
        wx.hideLoading();
      }
    });
  },
  openmap:function(){
    wx.openLocation({
      latitude: parseFloat(this.data.detail.latitude),
      longitude: parseFloat(this.data.detail.longitude),
      address: this.data.detail.address
    })
  },
  callme:function(){
    wx.makePhoneCall({
      phoneNumber: this.data.detail.mobile.toString(),
    });
  },
  get_tie: function () {
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_top_detail_tie',
      data: { uid: this.data.detail.uid },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ tie: d.data.data });
        } else {
          this.setData({ msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onDetail: function (e) {
    var id = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/index/detail/index?id=' + id + "&bottom=0",
    })
  },
  onIndex:function(){
    wx.switchTab({
      url: '/pages/shop/index',
    })
  },
  ongl:function(){
    wx.navigateTo({
      url: '/pages/shop/manage/index',
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})